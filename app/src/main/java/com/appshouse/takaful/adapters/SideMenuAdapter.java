package com.appshouse.takaful.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.attributes.SideMenu;
import com.appshouse.takaful.utilities.MyMethods;

import java.util.ArrayList;

/**
 * Created by Mohammed Algassab on 12/28/2015.
 */
public class SideMenuAdapter extends BaseAdapter {

    ArrayList<SideMenu> sideMenuArrayList;
    Context context;
    Typeface font;

    public SideMenuAdapter(Context context) {
        font = MyMethods.getRegularFont(context);
        sideMenuArrayList = new ArrayList<>();
        this.context = context;
    }

    public void add(SideMenu item) {
        sideMenuArrayList.add(item);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.row_side_menu, null);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            holder.ivIcon = (ImageView) convertView.findViewById(R.id.ivIcon);
            holder.lloRow = (LinearLayout) convertView.findViewById(R.id.lloRow);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        SideMenu sideMenu = sideMenuArrayList.get(position);
        holder.tvTitle.setText(sideMenu.mTitle);
        holder.tvTitle.setTypeface(font);
        holder.ivIcon.setImageResource(sideMenu.mImageId);
        holder.ivIcon.setColorFilter(context.getResources().getColor(R.color.side_menu_color), PorterDuff.Mode.MULTIPLY);

        return convertView;
    }

    @Override
    public int getCount() {
        return sideMenuArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class ViewHolder {
        public TextView tvTitle;
        public ImageView ivIcon;
        public LinearLayout lloRow;
    }
}
