package com.appshouse.takaful.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.MyTakafulDetailActivity;
import com.appshouse.takaful.activities.PaymentActivity;
import com.appshouse.takaful.activities.RenewDetailActivity;
import com.appshouse.takaful.attributes.MyTakaful;
import com.appshouse.takaful.constant.PolicyStatus;
import com.appshouse.takaful.constant.ProductCodes;
import com.appshouse.takaful.utilities.MyMethods;

import java.util.List;


/**
 * Created by Mohammed Algassab on 1/6/2016.
 */
public class MyTakafulAdapter extends RecyclerView.Adapter<MyTakafulAdapter.MyTakafulViewHolder> {

    private List<MyTakaful> myTakafulArrayList;
    private Context context;
    Typeface font;

    public static class MyTakafulViewHolder extends RecyclerView.ViewHolder {
        TextView tvMyTakafulNo;
        TextView tvDate;
        TextView tvButton;
        LinearLayout lloButton;
        LinearLayout bButton;
        RelativeLayout lloRow;

        public MyTakafulViewHolder(View itemView, Typeface font) {
            super(itemView);
            tvMyTakafulNo = (TextView) itemView.findViewById(R.id.tvMyTakafulNo);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            tvButton = (TextView) itemView.findViewById(R.id.tvButton);
            lloButton = (LinearLayout) itemView.findViewById(R.id.lloButton);
            lloRow = (RelativeLayout) itemView.findViewById(R.id.lloRow);
            bButton = (LinearLayout) itemView.findViewById(R.id.bButton);

            tvButton.setTypeface(font);
            tvDate.setTypeface(font);
            tvMyTakafulNo.setTypeface(font);
        }
    }

    public MyTakafulAdapter(Context context, List<MyTakaful> myTakafulArrayList) {
        this.myTakafulArrayList = myTakafulArrayList;
        this.context = context;
        font = MyMethods.getRegularFont(context);
    }

    @Override
    public MyTakafulViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_my_takaful, parent, false);
        return new MyTakafulViewHolder(rowView, font);
    }

    @Override
    public void onBindViewHolder(final MyTakafulViewHolder holder, int position) {
        final MyTakaful myTakaful = myTakafulArrayList.get(position);
        if (myTakaful.mStatus == PolicyStatus.ACTIVE) {
            holder.tvMyTakafulNo.setText(myTakaful.mPolicyNo);
        } else {
            holder.tvMyTakafulNo.setText(myTakaful.mQuotationNo);
        }
        holder.tvDate.setText(myTakaful.mStartDate + " - " + myTakaful.mExpireDate);

        if (myTakaful.mIsRenew) {
            holder.lloButton.setBackgroundColor(ContextCompat.getColor(context, R.color.my_takaful_button));
            holder.tvButton.setText(R.string.status_renew);
        } else if (myTakaful.mIsExpired) {
            holder.lloButton.setBackgroundColor(ContextCompat.getColor(context, R.color.policy_expired));
            holder.tvButton.setText(R.string.status_expired);
            holder.bButton.setEnabled(false);
            holder.bButton.setClickable(false);
        } else if ((myTakaful.mProductCode.contentEquals(ProductCodes.TRAVEL) ||
                myTakaful.mProductCode.contentEquals(ProductCodes.MOTOR)) && myTakaful.mStatus == PolicyStatus.APPROVED) {
            holder.lloButton.setBackgroundColor(ContextCompat.getColor(context, R.color.my_takaful_button));
            holder.tvButton.setText(R.string.status_issure);
        } else {
            holder.lloButton.setBackgroundColor(ContextCompat.getColor(context, R.color.my_takaful_disable_button));
            holder.bButton.setEnabled(false);
            holder.bButton.setClickable(false);

            switch (myTakaful.mStatus) {
                case PolicyStatus.DRAFT:
                    if (myTakaful.mProductCode.contentEquals(ProductCodes.MOTOR)) {
                        holder.lloButton.setBackgroundColor(ContextCompat.getColor(context, R.color.policy_pending));
                        holder.tvButton.setText(R.string.status_pending);
                    } else {
                        holder.tvButton.setText(R.string.quotation);
                    }
                    break;
                case PolicyStatus.REJECTED:
                    holder.tvButton.setText(R.string.status_reject);
                    break;
                case PolicyStatus.ACTIVE:
                    holder.tvButton.setText(R.string.status_active);
                    holder.lloButton.setBackgroundColor(ContextCompat.getColor(context, R.color.policy_active));
                    break;
                case PolicyStatus.RENEWED:
                    holder.tvButton.setText(R.string.status_renewed);
                    break;
                case PolicyStatus.SUSPENDED:
                    holder.tvButton.setText(R.string.status_suspended);
                    break;
                case PolicyStatus.CANCELLED:
                    holder.tvButton.setText(R.string.status_cancelled);
                    break;
                case PolicyStatus.MATURED:
                    holder.tvButton.setText(R.string.status_matured);
                    break;
                case PolicyStatus.FORFEITED:
                    holder.tvButton.setText(R.string.status_forfeited);
                    break;
                case PolicyStatus.LAPSED:
                    holder.tvButton.setText(R.string.status_lapsed);
                    break;
                default:
                    holder.tvButton.setText(" - ");
                    break;
            }
        }

        holder.bButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myTakaful.mIsExpired){
                    Log.d("isClick"  , "Expired");

                } else if ( myTakaful.mIsRenew ) {


                    Log.d("isClick","clickisrenew");
                    Log.d("isClick"  , "Renew");


                    Intent intent = new Intent(context, RenewDetailActivity.class);
                    intent.putExtra("no", myTakaful.mPolicyNo);
                    intent.putExtra("payFor", "renewal");

                    //Intent intent1 = new Intent(context , PaymentActivity.class) ;
                    //intent1.putExtra("no", myTakaful.mPolicyNo);
                    //intent1.putExtra("payFor", "renewal");


                    /**product code is sent to know if it is motor or not to handle on when
                     * the user press back
                     * @see PaymentActivity#onBackPressed() */

                    intent.putExtra("productCode", myTakaful.mProductCode);

                    context.startActivity(intent);
                    //intent1.putExtra("productCode", myTakaful.mProductCode);
                    //context.startActivity(intent1);

                } else if (myTakaful.mStatus == PolicyStatus.APPROVED && (myTakaful.mProductCode.contentEquals(ProductCodes.TRAVEL)
                        || myTakaful.mProductCode.contentEquals(ProductCodes.MOTOR))) {
                    //&& myTakaful.mStatus == PolicyStatus.RENEWED
                    //(myTakaful.mProductCode.contentEquals(ProductCodes.TRAVEL) ||
                    //                         myTakaful.mProductCode.contentEquals(ProductCodes.MOTOR))
                    //                        && myTakaful.mStatus == PolicyStatus.APPROVED
                    Intent intent = new Intent(context, PaymentActivity.class);
                    intent.putExtra("no", myTakaful.mQuotationNo);
                    intent.putExtra("payFor", "new");
                    context.startActivity(intent);
                }
            }
        });

        holder.lloRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyTakafulDetailActivity.class);
                intent.putExtra("myTakaful", myTakaful);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return myTakafulArrayList.size();
    }
}




