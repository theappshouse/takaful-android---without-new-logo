package com.appshouse.takaful.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.ProductDetailActivity;
import com.appshouse.takaful.attributes.Product;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.MyMethods;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Mohammed Algassab on 1/31/2016.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private List<Product> productList;
    private Context context;


    public static class ProductViewHolder extends RecyclerView.ViewHolder {
        TextView tvProductTitle, tvProductDesc;
        ImageView ivProduct;
        LinearLayout lloRow;

        public ProductViewHolder(View itemView, Context context) {
            super(itemView);
            tvProductTitle = (TextView) itemView.findViewById(R.id.tvProductTitle);
            tvProductDesc = (TextView) itemView.findViewById(R.id.tvProductDesc);
            tvProductTitle.setTypeface(MyMethods.getBoldFont(context));
            tvProductDesc.setTypeface(MyMethods.getRegularFont(context));
            ivProduct = (ImageView) itemView.findViewById(R.id.ivProduct);
            lloRow = (LinearLayout) itemView.findViewById(R.id.lloRow);
        }
    }

    public ProductAdapter(Context context, List<Product> productList) {
        this.productList = productList;
        this.context = context;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(
                viewType == 0 ? R.layout.row_product1 : R.layout.row_product2, parent, false);
        return new ProductViewHolder(rowView, context);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        final Product product = productList.get(position);
        holder.tvProductTitle.setText(product.mTitle);
        holder.tvProductDesc.setText(Html.fromHtml(product.mShotDesc));
        Picasso.with(context).load(APIs.ImageURL + product.mThumbnail).into(holder.ivProduct);
        holder.lloRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.putExtra("product", product);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    @Override
    public int getItemViewType(int position) {

        return (position % 2 == 0) ? 1 : 0;
    }
}

