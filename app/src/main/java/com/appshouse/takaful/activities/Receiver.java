package com.appshouse.takaful.activities;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Mohammed Algassab on 11/25/2014.
 */
public class Receiver extends ParsePushBroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        super.onReceive(context, intent);
        Log.i("Receiver", "onReceive");

    }

    //type 1 mean quotaiton rady for issue , type 2 mean policy is ready to be renewed,
    //type 3 there is claim notification, type 4 newsNotification, type 5 notification text
    @Override
    protected void onPushOpen(Context context, Intent intent) {
        Log.i("Receiver", "onPushOpen");

        try {
            final JSONObject jObject = new JSONObject(intent.getExtras().getString("com.parse.Data"));
            if (jObject.getInt("type") == 1 && jObject.getBoolean("approved")) {
                Intent i = new Intent(context, PaymentActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                i.putExtra("no", jObject.getString("quoteNo"));
                i.putExtra("payFor", "new");
                context.startActivity(i);
            } else if (jObject.getInt("type") == 2) {
                Intent i = new Intent(context, RenewDetailActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                i.putExtra("no", jObject.getString("policyNumber"));
                i.putExtra("productCode", jObject.getString("productCode"));
                context.startActivity(i);
            } else if (jObject.getInt("type") == 3) {
                Intent i = new Intent(context, ClaimDetailActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                i.putExtra("claimNo", jObject.getString("claimNo"));
                i.putExtra("getClaimDetail", true);
                context.startActivity(i);
            } else if (jObject.getInt("type") == 4) {
                Intent i = new Intent(context, NewsDetailActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                Log.i("newsId", jObject.getInt("newsId") + "");
                i.putExtra("newsId", jObject.getInt("newsId"));
                context.startActivity(i);
            } else if (jObject.getInt("type") == 5) {
                Intent i = new Intent(context, NotificationDetailActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                i.putExtra("notificationText", jObject.getString("alert"));
                context.startActivity(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);
        Log.i("Receiver", "onPushReceive");
    }
}

