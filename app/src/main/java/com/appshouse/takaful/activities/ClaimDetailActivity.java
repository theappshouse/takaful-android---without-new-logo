package com.appshouse.takaful.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 1/13/2016.
 */
public class ClaimDetailActivity extends AppCompatActivity {
    Toolbar toolbar;
    LinearLayout lloClaim;
    TextView tvClaimNo, tvStatus, tvReasonRejection;
    LinearLayout lloStatus;
    View view;
    String claimNo, claimDetail, reasonRejection;
    int status;
    TextView tvPolicyNo, tvDateOfLoss, tvTimeOfLoss, tvPolicyReportNo, tvPoliceReportedDate, tvClaimDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claim_detail);
        view = findViewById(android.R.id.content);
        lloClaim = (LinearLayout) findViewById(R.id.lloClaim);
        tvClaimNo = (TextView) findViewById(R.id.tvClaimNo);
        tvReasonRejection = (TextView) findViewById(R.id.tvReasonRejection);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        tvPolicyNo = (TextView) findViewById(R.id.tvPolicyNo);
        tvDateOfLoss = (TextView) findViewById(R.id.tvDateOfLoss);
        tvTimeOfLoss = (TextView) findViewById(R.id.tvTimeOfLoss);
        tvPolicyReportNo = (TextView) findViewById(R.id.tvPolicyReportNo);
        tvPoliceReportedDate = (TextView) findViewById(R.id.tvPoliceReportedDate);
        tvClaimDate = (TextView) findViewById(R.id.tvClaimDate);
        lloStatus = (LinearLayout) findViewById(R.id.lloStatus);

        //Create and set the action bar
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.claim_detail);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        claimNo = getIntent().getStringExtra("claimNo");

        if (!getIntent().getBooleanExtra("getClaimDetail", false)) {
            status = getIntent().getIntExtra("status", 0);

            reasonRejection = getIntent().getStringExtra("reasonRejection");
            claimDetail = getIntent().getStringExtra("claimDetails");
            setClaimDetails();
        } else
            getSingleClaim();
    }

    String Tag_Request = "getSingleClaim";

    private void getSingleClaim() {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetSingleClaim
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                try {
                    Log.i("Claim", response.toString());
                    JSONObject claimObject = response.getJSONObject("result");
                    status = claimObject.getInt("Status");

                    reasonRejection = claimObject.getString("Rejection_Reason");

                    claimObject.remove("Claim No");
                    claimObject.remove("Status");
                    claimObject.remove("Rejection_Reason");
                    claimObject.remove("Nature of loss");

                    claimDetail = claimObject.toString();
                    setClaimDetails();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showNetworkErrorDialog(ClaimDetailActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getSingleClaim();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SharedPreferencesClass setting = new SharedPreferencesClass(ClaimDetailActivity.this);
                Log.i("userId", String.valueOf(setting.getUserId()));
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("cpr", setting.getCpr());
                params.put("claimNo", claimNo);
                params.put("deviceId", MyMethods.getDeviceId(ClaimDetailActivity.this));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setClaimDetails() {
        tvClaimNo.setText(getIntent().getStringExtra("claimNo"));

        switch (status) {
            case 1:
                tvStatus.setText(R.string.status_claim_pending);
                lloStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.policy_pending));
                break;
            case 2:
                tvStatus.setText(R.string.status_claim_accepted);
                lloStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.status_issued));
                break;
            case 3:
                tvStatus.setText(R.string.status_claim_pending);
                lloStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.policy_expired));
                if (!reasonRejection.isEmpty()) {
                    tvReasonRejection.setVisibility(View.VISIBLE);
                    tvReasonRejection.setText(reasonRejection);
                }
                break;
        }

        try {
            JSONObject jsonObject = new JSONObject(claimDetail);
            tvPolicyNo.setText(jsonObject.getString("Policy No"));
            tvDateOfLoss.setText(jsonObject.getString("Date of loss"));
            tvTimeOfLoss.setText(jsonObject.getString("Time of loss"));
            tvPolicyReportNo.setText(jsonObject.getString("Police report no"));
            tvPoliceReportedDate.setText(jsonObject.getString("Police reported date"));
            tvClaimDate.setText(jsonObject.getString("Claim date"));

            //old function
//            Iterator<String> iterator = jsonObject.keys();
//            /**
//             * We create the textViews dynamically depend on the jsonObject value
//             * @see #createTitleTextView(String)  for creating the title
//             * @see #createInfoTextView(String)  for creating the info of the title
//             **/
//            while (iterator.hasNext()) {
//
//                String key = iterator.next();
//                String value = jsonObject.getString(key);
//                if (!value.isEmpty()) {
//                    createTitleTextView(key);
//                    createInfoTextView(value);
//                }
//            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setDemoData() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Claim No.", "23423423423423");
            jsonObject.put("Policy No.", "12416574354");
            jsonObject.put("Car No.", "641532");
            jsonObject.put("Date of Loss No", "01/10/2015");
            jsonObject.put("Time of Loss", "13:25:48");
            jsonObject.put("Police Report No.", "23 1216");

            Iterator<String> iterator = jsonObject.keys();
            while (iterator.hasNext()) {
                String key = iterator.next();
                createTitleTextView(key);
                String value = jsonObject.getString(key);
                createInfoTextView(value);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void createTitleTextView(String title) {
        TextView tvClaimTitle = new TextView(this);
        tvClaimTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textSize_claimDetailTitles));
        tvClaimTitle.setTextColor(ContextCompat.getColor(this, R.color.titleText));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, getResources().getDimensionPixelOffset(R.dimen.marginTop_claimDetailTitles), 0, 0);
        tvClaimTitle.setLayoutParams(layoutParams);
        tvClaimTitle.setText(title);
        tvClaimTitle.setTypeface(MyMethods.getRegularFont(ClaimDetailActivity.this));
        lloClaim.addView(tvClaimTitle);
    }

    private void createInfoTextView(String info) {
        TextView tvClaimInfo = new TextView(this);
        tvClaimInfo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textSize_claimDetailInfo));
        tvClaimInfo.setTextColor(ContextCompat.getColor(this, R.color.textInfo));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout
                .LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, getResources().getDimensionPixelOffset(R.dimen.marginTop_claimDetailInfo), 0, 0);
        tvClaimInfo.setLayoutParams(layoutParams);
        tvClaimInfo.setText(info);
        tvClaimInfo.setTypeface(MyMethods.getRegularFont(ClaimDetailActivity.this));
        lloClaim.addView(tvClaimInfo);
    }
}
