package com.appshouse.takaful.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.adapters.AgencyAdapter;
import com.appshouse.takaful.attributes.Agency;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Mohammed Algassab on 2/6/2016.
 */
public class AgencyActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView lvAgency;
    ArrayList<Agency> agencyArrayList;
    RecyclerView.Adapter agencyAdapter;
    View view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_agency);
        view = findViewById(android.R.id.content);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_agencies);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        lvAgency = (RecyclerView) findViewById(R.id.lvAgency);
        lvAgency.setLayoutManager(new LinearLayoutManager(this));
        lvAgency.setHasFixedSize(true);
        //setAgencyData();
        getAgency();
    }

    String Tag_Request = "get_agency";

    private void getAgency() {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.GET, APIs.GetAgency
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                setAgencyData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("VolleyError", error.toString());
                MyMethods.showNetworkErrorDialog(AgencyActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getAgency();
                    }
                });
            }
        });
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setAgencyData(JSONObject jsonObject) {
        try {
            Log.i("setAgencyData", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                agencyArrayList = new ArrayList<>();
                JSONArray resultArray = jsonObject.getJSONArray("result");

                for (int i = 0; i < resultArray.length(); i++) {
                    JSONObject agencyObject = resultArray.getJSONObject(i);
                    Agency agency = new Agency(agencyObject.getString("Logo"), agencyObject.getString("Car_Name"),
                            agencyObject.getString("Agency_Name"), agencyObject.getString("Address"),
                            agencyObject.getString("Phone"), agencyObject.getString("Showroom_Office_Days"),
                            agencyObject.getString("Showroom_Office_Hours"), agencyObject.getString("Services_Office_Days"),
                            agencyObject.getString("Services_Office_Hours"),agencyObject.getDouble("Latitude"),
                            agencyObject.getDouble("Longitude"));
                    agencyArrayList.add(agency);
                }

                agencyAdapter = new AgencyAdapter(AgencyActivity.this, agencyArrayList);
                lvAgency.setAdapter(agencyAdapter);
            } else {
                MyMethods.showNetworkErrorDialog(AgencyActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getAgency();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
