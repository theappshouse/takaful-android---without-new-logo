package com.appshouse.takaful.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.utilities.MyMethods;

/**
 * Created by Mohammed Algassab on 1/13/2016.
 */
public class NotificationDetailActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView tvNotification;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        view = findViewById(android.R.id.content);
        tvNotification = (TextView) findViewById(R.id.tvNotification);

        //Create and set the action bar
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.notification);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        tvNotification.setText(getIntent().getStringExtra("notificationText"));
    }
}
