package com.appshouse.takaful.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 1/31/2016.
 */
public class QuoteBaitakActivity extends AppCompatActivity {
    Toolbar toolbar;
    EditText etHousehold, etJewelery, etFixtures, etFurniture;
    EditText etPolicyStartDay, etS1Building;

    LinearLayout lloPolicyStartPickDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote_baitak);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_getBaitakQuote);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        etHousehold = (EditText) findViewById(R.id.etHousehold);
        etJewelery = (EditText) findViewById(R.id.etJewelery);
        etFixtures = (EditText) findViewById(R.id.etFixtures);
        etFurniture = (EditText) findViewById(R.id.etFurniture);
        etPolicyStartDay = (EditText) findViewById(R.id.etPolicyStartDay);
        etS1Building = (EditText) findViewById(R.id.etS1Building);

        lloPolicyStartPickDate = (LinearLayout) findViewById(R.id.lloPolicyStartPickDate);

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));
        lloPolicyStartPickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etPolicyStartDay.requestFocus();
                openPickDateDialog();
            }
        });
        //setAutoDemoData();
    }


    private void openPickDateDialog() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String mYear, mMonth, mDay;
                mYear = String.valueOf(year);
                mMonth = String.valueOf(monthOfYear + 1);
                mDay = String.valueOf(dayOfMonth);
                etPolicyStartDay.setText(mDay + "/" + mMonth + "/" + mYear);
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    private void setAutoDemoData() {
        etHousehold.setText("1000");
        etJewelery.setText("2500");
        etFixtures.setText("1300");
        etFurniture.setText("6000");
    }

    String household, jewelery, fixtures, furniture;
    String policyStartDate, S1Building;

    private void prepareSendQuote() {
        //get all the details in string
        household = etHousehold.getText().toString();
        jewelery = etJewelery.getText().toString();
        fixtures = etFixtures.getText().toString();
        furniture = etFurniture.getText().toString();
        policyStartDate = etPolicyStartDay.getText().toString();
        S1Building = etS1Building.getText().toString();


        if (policyStartDate.isEmpty() || S1Building.isEmpty()) {
            Snackbar.make(findViewById(R.id.main), R.string.message_enterRequiredData, Snackbar.LENGTH_INDEFINITE).show();
        } else {
            sendQuote();
        }
    }

    private void sendQuote() {
        String Tag_Request = "sendQuote";
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetSaveBaitakQuote
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                getSendQuoteResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Snackbar.make(findViewById(R.id.main), R.string.networkError, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                sendQuote();
                            }
                        }).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                Log.i("household", household);
                Log.i("jewelery", jewelery);
                Log.i("fixtures", fixtures);
                Log.i("furniture", furniture);
                Log.i("S1Building", S1Building);


                params.put("household", household);
                params.put("jewelery", jewelery);
                params.put("fixtures", fixtures);
                params.put("furniture", furniture);
                params.put("S1Building", S1Building);
                params.put("saveQuote", "0");

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void getSendQuoteResult(JSONObject jsonObject) {
        try {
            Log.i("getSendQuoteResult", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                double premium = jsonObject.getDouble("result");
                double sumAssured = jsonObject.getDouble("totalSA");
                Intent intent = new Intent(this, SaveQuoteBaitakActivity.class);
                intent.putExtra("household", household);
                intent.putExtra("jewelery", jewelery);
                intent.putExtra("fixtures", fixtures);
                intent.putExtra("furniture", furniture);
                intent.putExtra("policyStartDate",policyStartDate );
                intent.putExtra("S1Building", S1Building);
                intent.putExtra("cpr", getIntent().getStringExtra("cpr"));
                intent.putExtra("premium", premium);
                intent.putExtra("sumAssured", sumAssured);
                startActivity(intent);
            } else {
                Snackbar.make(findViewById(R.id.main),  R.string.networkError,Snackbar.LENGTH_INDEFINITE).
                        setAction(R.string.dialog_button_default, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        }).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                MyMethods.hideKeyboard(this);
                View view = View.inflate(this, R.layout.dialog_term_condition, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                final CheckBox checkBox = (CheckBox) view.findViewById(R.id.cbTermAccept);
                builder.setView(view)
                        .setCancelable(false)
                        .setPositiveButton(R.string.dialog_button_continue, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (checkBox.isChecked()) {
                                    prepareSendQuote();
                                } else {
                                    Toast.makeText(QuoteBaitakActivity.this, R.string.message_mustAcceptTerm, Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
