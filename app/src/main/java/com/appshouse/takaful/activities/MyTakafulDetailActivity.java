package com.appshouse.takaful.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.adapters.PagerAdapter;
import com.appshouse.takaful.attributes.MyTakaful;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.constant.PolicyStatus;
import com.appshouse.takaful.constant.ProductCodes;
import com.appshouse.takaful.fragments.TakafulFireBaitakCardFragment;
import com.appshouse.takaful.fragments.TakafulMotorCardFragment;
import com.appshouse.takaful.fragments.TakafulTravelCardFragment;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 1/9/2016.
 */
public class MyTakafulDetailActivity extends AppCompatActivity {

    ViewPager viewPager;
    Toolbar toolbar;
    ImageView ivEngIndicator, ivArIndicator, ivMaximize, ivSave;
    MyTakaful myTakaful;
    TextView tvDescription, tvMembers, tvMyTakafulNo, tvDate, tvStatus, tvStatusText, tvQuotationTitle;
    LinearLayout lloStatus, bStatus;
    int WRITE_EXTERNAL = 2;
    LinearLayout lloQuotationDetail;
    ProgressBar pbQuotationDetail;
    Typeface fontRegular,fontBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_takaful_detail);

        //Create and set the action bar
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_TakafulDetail);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fontRegular = MyMethods.getRegularFont(this);
        fontBold = MyMethods.getBoldFont(this);
        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, fontRegular);

        lloQuotationDetail = (LinearLayout) findViewById(R.id.lloQuotationDetail);
        ivEngIndicator = (ImageView) findViewById(R.id.ivEngIndicator);
        ivArIndicator = (ImageView) findViewById(R.id.ivArIndicator);
        ivMaximize = (ImageView) findViewById(R.id.ivMaximize);
        ivSave = (ImageView) findViewById(R.id.ivSave);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        tvMyTakafulNo = (TextView) findViewById(R.id.tvMyTakafulNo);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        tvMembers = (TextView) findViewById(R.id.tvMembers);
        lloStatus = (LinearLayout) findViewById(R.id.lloStatus);
        bStatus = (LinearLayout) findViewById(R.id.bStatus);
        pbQuotationDetail = (ProgressBar) findViewById(R.id.pbQuotaitonDetail);
        tvStatusText = ((TextView) findViewById(R.id.tvStatusText));
        tvQuotationTitle = ((TextView) findViewById(R.id.tvQuotationTitle));

        viewPager.setOffscreenPageLimit(2);
        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        ivEngIndicator.setImageResource(R.drawable.shape_card_indicator_selected);
                        ivArIndicator.setImageResource(R.drawable.shape_card_indicator);
                        break;
                    case 1:
                        ivEngIndicator.setImageResource(R.drawable.shape_card_indicator);
                        ivArIndicator.setImageResource(R.drawable.shape_card_indicator_selected);
                        break;
                }
            }
        });

        myTakaful = getIntent().getParcelableExtra("myTakaful");
        ivMaximize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (myTakaful.mProductCode.contentEquals(ProductCodes.MOTOR)) {
                    intent = new Intent(MyTakafulDetailActivity.this, MaxMotorCardActivity.class);
                } else if (myTakaful.mProductCode.contentEquals(ProductCodes.FIRE) ||
                        myTakaful.mProductCode.contentEquals(ProductCodes.BAITAK)) {
                    intent = new Intent(MyTakafulDetailActivity.this, MaxFireBaitakCardActivity.class);
                } else if (myTakaful.mProductCode.contentEquals(ProductCodes.TRAVEL)) {
                    intent = new Intent(MyTakafulDetailActivity.this, MaxTravelCardActivity.class);
                }
                intent.putExtra("myTakaful", getIntent().getParcelableExtra("myTakaful"));
                startActivity(intent);
            }
        });

        Log.i("Takaful status",myTakaful.mStatus+"");
        if (myTakaful.mStatus == PolicyStatus.ACTIVE) {
            switch (myTakaful.mProductCode) {
                case ProductCodes.MOTOR:
                    getCard(APIs.GetMotorCardDetail);
                    break;
                case ProductCodes.TRAVEL:
                    getCard(APIs.GetTravelCardDetail);
                    break;
                case ProductCodes.FIRE:
                case ProductCodes.BAITAK:
                    getCard(APIs.GetFireBaitakCardDetail);
                    break;
            }
        } else {
            findViewById(R.id.vCardView).setVisibility(View.GONE);
        }

        getQuotationDetail();
        tvDate.setText(myTakaful.mStartDate + " - " + myTakaful.mExpireDate);

        if (myTakaful.mIsRenew) {
            lloStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.my_takaful_button));
            tvStatus.setText(R.string.status_renew);
            //((TextView) findViewById(R.id.tvStatusText)).setText(tvStatus.getText().toString());
        } else if (myTakaful.mIsExpired) {
            lloStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.policy_expired));
            tvStatus.setText(R.string.status_expired);
            bStatus.setEnabled(false);
            bStatus.setClickable(false);
        } else if ((myTakaful.mProductCode.contentEquals(ProductCodes.TRAVEL) ||
                myTakaful.mProductCode.contentEquals(ProductCodes.MOTOR)) && myTakaful.mStatus == PolicyStatus.APPROVED) {
            lloStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.my_takaful_button));
            tvStatus.setText(R.string.status_issure);
            tvStatusText.setText(R.string.quotation);
        } else {
            lloStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.my_takaful_disable_button));
            bStatus.setEnabled(false);
            bStatus.setClickable(false);

            switch (myTakaful.mStatus) {
                case PolicyStatus.DRAFT:
                    tvStatus.setText(myTakaful.mProductCode.contentEquals(ProductCodes.MOTOR) ? getString(R.string.pending) : getString(R.string.quotation));
                    lloStatus.setBackgroundColor(ContextCompat.getColor(this, myTakaful.mProductCode.contentEquals(
                            ProductCodes.MOTOR) ? R.color.policy_pending : R.color.my_takaful_disable_button));

                    tvStatusText.setText(myTakaful.mProductCode.contentEquals(ProductCodes.MOTOR)
                            ? getString(R.string.pending_takaful_approval) : getString(R.string.quotation));
                    break;
                case PolicyStatus.REJECTED:
                    tvStatus.setText(R.string.status_reject);
                    break;
                case PolicyStatus.ACTIVE:
                    tvStatus.setText(R.string.status_active);
                    lloStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.policy_active));
                    break;
                case PolicyStatus.RENEWED:
                    tvStatus.setText(R.string.status_renewed);
                    break;
                case PolicyStatus.SUSPENDED:
                    tvStatus.setText(R.string.status_suspended);
                    break;
                case PolicyStatus.CANCELLED:
                    tvStatus.setText(R.string.status_cancelled);
                    break;
                case PolicyStatus.MATURED:
                    tvStatus.setText(R.string.status_matured);
                    break;
                case PolicyStatus.FORFEITED:
                    tvStatus.setText(R.string.status_forfeited);
                    break;
                case PolicyStatus.LAPSED:
                    tvStatus.setText(R.string.status_lapsed);
                    break;
                default:
                    tvStatus.setText(" - ");
                    break;
            }
        }

        bStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myTakaful.mIsRenew) {
                    Intent intent = new Intent(MyTakafulDetailActivity.this, RenewDetailActivity.class);
                    intent.putExtra("no", myTakaful.mPolicyNo);
                    //intent.putExtra("payFor", "renewal");

                    /**product code is sent to know if it is motor or not to handle on when
                     * the user press back
                     * @see PaymentActivity#onBackPressed() */

                    intent.putExtra("productCode", myTakaful.mProductCode);
                    startActivity(intent);
                } else if ((myTakaful.mProductCode.contentEquals(ProductCodes.TRAVEL) ||
                        myTakaful.mProductCode.contentEquals(ProductCodes.MOTOR))
                        && myTakaful.mStatus == PolicyStatus.APPROVED) {
                    Intent intent = new Intent(MyTakafulDetailActivity.this, PaymentActivity.class);

                    intent.putExtra("no", myTakaful.mQuotationNo);
                    intent.putExtra("payFor", "new");
                    startActivity(intent);
                }
            }
        });

        ivSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MyTakafulDetailActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    MyMethods.rationalDialog(MyTakafulDetailActivity.this, R.string.dialog_message_permStorage, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MyTakafulDetailActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL);
                        }
                    });
                } else {
                    ActivityCompat.requestPermissions(MyTakafulDetailActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL);
                }
            }
        });

        if (myTakaful.mStatus == PolicyStatus.ACTIVE)
            tvMyTakafulNo.setText(myTakaful.mPolicyNo);
        else
            tvMyTakafulNo.setText(myTakaful.mQuotationNo);

        ((TextView) findViewById(R.id.tvProductName)).setText(myTakaful.mProductName);
        ((TextView) findViewById(R.id.tvQuotationNo)).setText(myTakaful.mQuotationNo);
        ((TextView) findViewById(R.id.tvPolicyNo)).setText(myTakaful.mPolicyNo);
        ((TextView) findViewById(R.id.tvPolicyStartDate)).setText(myTakaful.mStartDate);
        ((TextView) findViewById(R.id.tvPolicyExpiryDate)).setText(myTakaful.mExpireDate);
        ((TextView) findViewById(R.id.tvPolicyInceptionDate)).setText(myTakaful.mPolicyInceptionDate);
        ((TextView) findViewById(R.id.tvPolicyGrossPremium)).setText(myTakaful.mPolicyGrossPremium);
        ((TextView) findViewById(R.id.tvPolicyNetPremium)).setText(getString(R.string.bd) + myTakaful.mPolicyNetPremium);
        ((TextView) findViewById(R.id.tvPolicySumInsured)).setText(getString(R.string.bd) + myTakaful.mPolicySumInsured);

        if (tvStatusText.getText().toString().isEmpty())
            tvStatusText.setText(tvStatus.getText().toString());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == WRITE_EXTERNAL) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                ivSave.setVisibility(View.GONE);
                ivMaximize.setVisibility(View.GONE);
                View view = findViewById(R.id.ivCard);
                view.setDrawingCacheEnabled(true);
                Bitmap b = view.getDrawingCache();

                if (MyMethods.saveImageToExternalStorage(this, b, myTakaful.mProductCode + "_" + myTakaful.mPolicyNo.replaceAll("/", "-") + "_" + System.currentTimeMillis())) {
                    Snackbar.make(view, R.string.message_card_saved_success, Snackbar.LENGTH_INDEFINITE).
                            setAction(R.string.dialog_button_default, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                }
                            }).show();
                } else {
                    Snackbar.make(view, R.string.message_card_saved_failed, Snackbar.LENGTH_LONG).show();
                }
                ivSave.setVisibility(View.VISIBLE);

                if (!myTakaful.mProductCode.contentEquals(ProductCodes.TRAVEL))
                    ivMaximize.setVisibility(View.VISIBLE);
            } else if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                MyMethods.rationalDialog(this, R.string.dialog_message_permStorage, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 2);
                    }
                });
            }
        }
    }

    String Tag_Request_Quotation = "getQuotation";

    private void getQuotationDetail() {
        //pbQuotationDetail.setVisibility(View.VISIBLE);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetQuotationDetail
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pbQuotationDetail.setVisibility(View.GONE);
                setQuotationDetail(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pbQuotationDetail.setVisibility(View.GONE);
                MyMethods.showNetworkErrorDialog(MyTakafulDetailActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getQuotationDetail();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SharedPreferencesClass setting = new SharedPreferencesClass(MyTakafulDetailActivity.this);
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("deviceId", MyMethods.getDeviceId(MyTakafulDetailActivity.this));
                params.put("cpr", setting.getCpr());
                params.put("quotationNo", myTakaful.mQuotationNo);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request_Quotation);
    }

    private void setQuotationDetail(JSONObject response) {
        Log.i("setQuotationDetail",response.toString());
        try {
            JSONObject resultObject = response.getJSONObject("result");
//            JSONObject generalObject = resultObject.getJSONObject("GeneralDetails");
//            Iterator<String> iterator = generalObject.keys();
//
//            while (iterator.hasNext()) {
//                String key = iterator.next();
//                String value = generalObject.getString(key);
//                if (!value.isEmpty()) {
//                    createTitleTextView(key);
//                    createInfoTextView(value);
//                }
//            }

            JSONArray generalDetails = resultObject.getJSONArray("GeneralDetails");
            for (int i = 0; i < generalDetails.length(); i++) {
                JSONObject generalObject = generalDetails.getJSONObject(i);
                createTitleTextView(generalObject.getString(getString(R.string.z_title)));
                createInfoTextView(generalObject.getString(getString(R.string.z_value)));
            }

            switch (myTakaful.mProductCode) {
                case ProductCodes.MOTOR:
                    findViewById(R.id.tvMotorNote).setVisibility(View.VISIBLE);
                    if (resultObject.has("Addtional Covers")) {
//                        JSONArray additionalCoverArray = resultObject.getJSONArray("Addtional Covers");
//                        createTitleTextView(getString(R.string.additional_covers));
//                        for (int i = 0; i < additionalCoverArray.length(); i++) {
//                            createInfoTextView(additionalCoverArray.getString(i));
//                        }


                        JSONObject additionalCoverObject = resultObject.getJSONObject("Addtional Covers");
                        createTitleTextView(getString(R.string.additional_covers));
                        JSONArray additionalCoverArray = additionalCoverObject.getJSONArray(getString(R.string.z_language));
                        for (int i = 0; i < additionalCoverArray.length(); i++) {
                            createInfoTextView(additionalCoverArray.getString(i));
                        }
                    }
                    break;

                case ProductCodes.TRAVEL:
                    tvQuotationTitle.setVisibility(View.VISIBLE);

                    if (resultObject.has("Travel Memebers")) {
//                        JSONArray travelMemberArray = resultObject.getJSONArray("Travel Memebers");
//                        createTitleTextView(getString(R.string.travel_members));
//                        for (int i = 0; i < travelMemberArray.length(); i++) {
//                            JSONObject travelMemberObject = travelMemberArray.getJSONObject(i);
//                            Iterator<String> travelMemberIterator = travelMemberObject.keys();
//                            createInfoTextView("");
//                            while (travelMemberIterator.hasNext()) {
//                                String key = travelMemberIterator.next();
//                                String value = travelMemberObject.getString(key);
//                                if (!value.isEmpty()) {
//                                    createInfoTextView(key + ": " + value);
//                                }
//                            }
//                        }


                        JSONArray allTravelMemberArray = resultObject.getJSONArray("Travel Memebers");
                        createInfoTextView("");
                        createTitleTextView(getString(R.string.travel_members));
                        for (int i = 0; i < allTravelMemberArray.length(); i++) {
                            JSONArray travelMemberDetailArray = allTravelMemberArray.getJSONArray(i);

                            createInfoTextView("");

                            for (int t = 0; t < travelMemberDetailArray.length(); t++) {
                                JSONObject travelMemberDetailObject = travelMemberDetailArray.getJSONObject(t);
                                if (!travelMemberDetailObject.getString(getString(R.string.z_value)).isEmpty())
                                    createInfoTextView(travelMemberDetailObject.getString(getString(R.string.z_title))
                                            + ": " + travelMemberDetailObject.getString(getString(R.string.z_value)));
                            }
                        }
                    }
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void createTitleTextView(String title) {
        TextView tvClaimTitle = new TextView(this);
        tvClaimTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textSize_myTakafulDesc));
        tvClaimTitle.setTextColor(ContextCompat.getColor(this, R.color.titleText));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, getResources().getDimensionPixelOffset(R.dimen.marginTop_myTakafulDesc), 0, 0);
        tvClaimTitle.setLayoutParams(layoutParams);
        tvClaimTitle.setText(title);
        tvClaimTitle.setTypeface(fontRegular);

        lloQuotationDetail.addView(tvClaimTitle);
    }

    private void createInfoTextView(String info) {
        TextView tvClaimInfo = new TextView(this);
        tvClaimInfo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textSize_myTakafulDesc));
        tvClaimInfo.setTextColor(ContextCompat.getColor(this, R.color.textInfo));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout
                .LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tvClaimInfo.setLayoutParams(layoutParams);
        tvClaimInfo.setText(info);
        tvClaimInfo.setTypeface(fontRegular);
        lloQuotationDetail.addView(tvClaimInfo);
    }

    String Tag_Request = "getCard";

    private void getCard(final String url) {

        findViewById(R.id.vCard).setVisibility(View.INVISIBLE);
        findViewById(R.id.lyLoading).setVisibility(View.VISIBLE);

        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, url
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                findViewById(R.id.vCard).setVisibility(View.VISIBLE);
                findViewById(R.id.lyLoading).setVisibility(View.GONE);

                switch (myTakaful.mProductCode) {
                    case ProductCodes.MOTOR:
                        setMotorCard(response);
                        break;
                    case ProductCodes.TRAVEL:
                        setTravelCard(response);
                        break;
                    case ProductCodes.FIRE:
                    case ProductCodes.BAITAK:
                        setFireBaitakCard(response);
                        break;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showNetworkErrorDialog(MyTakafulDetailActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getCard(url);
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SharedPreferencesClass setting = new SharedPreferencesClass(MyTakafulDetailActivity.this);
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("deviceId", MyMethods.getDeviceId(MyTakafulDetailActivity.this));
                params.put("cpr", setting.getCpr());
                params.put("policyNumber", myTakaful.mPolicyNo);
                params.put("quotationNo", myTakaful.mQuotationNo);
                params.put("productCode", myTakaful.mProductCode);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    /**
     * We set the card by creating two fragment depend on the type one from english and other for arabic
     * then we set them in the viewPager in this activity
     *
     * @see #setMotorCard(JSONObject)
     * @see #setTravelCard(JSONObject)
     * @see #setFireBaitakCard(JSONObject)
     */
    private void setMotorCard(JSONObject jsonObject) {
        try {
            Log.i("setMotorCard", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                JSONObject resultObject = jsonObject.getJSONObject("result");
                if (resultObject.getInt("errCode") == 0) {

                    String cpr = resultObject.getString("CPRNumber");
                    String commencement = resultObject.getString("commencement");
                    String cover = resultObject.getString("coverName");
                    String expiryDate = resultObject.getString("expiryDate");
                    String makeAndModel = resultObject.getString("makeAndModel");
                    String regNumber = resultObject.getString("regNumberChasisNumber");
                    String excessAmt = resultObject.getString("excessAmt");
                    String dateOfIssue = resultObject.getString("dateOfIssue");
                    String addlBenefits = resultObject.getString("addlBenefits");

                    myTakaful.addMotorCardInfo(cpr, commencement, cover, expiryDate, makeAndModel,
                            regNumber, excessAmt, dateOfIssue, addlBenefits);

                    TakafulMotorCardFragment takafulMotorCardFragment = new TakafulMotorCardFragment();
                    Bundle argument = new Bundle();
                    argument.putString("name", myTakaful.mPolicyHolderName);
                    argument.putString("firstNumber", myTakaful.mPolicyNo);
                    argument.putString("finalNumber", " - " + myTakaful.mRegNumber);

                    argument.putString("commencementDate", myTakaful.mCommencement.split(" ")[0]);
                    if (myTakaful.mCommencement.contains(" "))
                        argument.putString("commencementTime", myTakaful.mCommencement.split(" ")[1]);

                    if (myTakaful.mExpiryDate.contains(" ")) {
                        argument.putString("expiryDate", myTakaful.mExpiryDate.split(" ")[0]);
                        argument.putString("expiryTime", myTakaful.mExpiryDate.split(" ")[1]);
                    } else {
                        argument.putString("expiryDate", myTakaful.mExpiryDate);
                    }

                    if (myTakaful.mMakeAndModel.contains("/")) {
                        argument.putString("vehicleYear", myTakaful.mMakeAndModel.split("/")[0]);
                        argument.putString("vehicleType", myTakaful.mMakeAndModel.split("/")[1]);
                    } else {
                        argument.putString("vehicleType", myTakaful.mMakeAndModel);
                    }

                    argument.putString("cover", myTakaful.mCover);
                    takafulMotorCardFragment.setArguments(argument);

                    TakafulMotorCardFragment takafulMotorCardFragmentAr = new TakafulMotorCardFragment();
                    Bundle argumentAR = new Bundle();
                    argumentAR.putBoolean("ar", true);
                    argumentAR.putString("name", myTakaful.mPolicyHolderName);
                    argumentAR.putString("firstNumber", myTakaful.mPolicyNo);
                    argumentAR.putString("finalNumber", " - " + myTakaful.mRegNumber);
                    argumentAR.putString("commencementDate", myTakaful.mCommencement.split(" ")[0]);
                    if (myTakaful.mCommencement.contains(" "))
                        argumentAR.putString("commencementTime", myTakaful.mCommencement.split(" ")[1]);

                    if (myTakaful.mExpiryDate.contains(" ")) {
                        argumentAR.putString("expiryDate", myTakaful.mExpiryDate.split(" ")[0]);
                        argumentAR.putString("expiryTime", myTakaful.mExpiryDate.split(" ")[1]);
                    } else {
                        argumentAR.putString("expiryDate", myTakaful.mExpiryDate);
                    }

                    if (myTakaful.mMakeAndModel.contains("/")) {
                        argumentAR.putString("vehicleYear", myTakaful.mMakeAndModel.split("/")[0]);
                        argumentAR.putString("vehicleType", myTakaful.mMakeAndModel.split("/")[1]);
                    } else {
                        argumentAR.putString("vehicleType", myTakaful.mMakeAndModel);
                    }

                    argumentAR.putString("cover", myTakaful.mCover);
                    takafulMotorCardFragmentAr.setArguments(argumentAR);

                    PagerAdapter mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
                    mPagerAdapter.addFragment(takafulMotorCardFragment);
                    mPagerAdapter.addFragment(takafulMotorCardFragmentAr);
                    viewPager.setAdapter(mPagerAdapter);
                }

            } else if (jsonObject.getInt("status") == 4) {
                MyMethods.showLoginPage(this);
            } else {
                MyMethods.showNetworkErrorDialog(this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getCard(APIs.GetMotorCardDetail);
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setTravelCard(JSONObject jsonObject) {
        try {
            Log.i("setTravelCard", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                JSONObject resultObject = jsonObject.getJSONObject("result");
                String planType = resultObject.getString("PlanType");
                String maxLimit = resultObject.getString("MaxLimit");
                String policyStart = resultObject.getString("PolicyStart");
                String policyEnd = resultObject.getString("PolicyEnd");
                String passport = resultObject.getString("Passport");

                myTakaful.addTravelCardInfo(planType, maxLimit, policyStart, policyEnd, passport);

                TakafulTravelCardFragment takafulTravelCardFragment = new TakafulTravelCardFragment();
                Bundle argument = new Bundle();
                argument.putString("name", myTakaful.mPolicyHolderName);
                argument.putString("firstNumber", myTakaful.mPolicyNo);
                argument.putString("finalNumber", " - " + myTakaful.mPassport);
                argument.putString("policyStartDate", myTakaful.mPolicyStart);
                argument.putString("policyEndDate", myTakaful.mPolicyEnd);
                argument.putString("planType", myTakaful.mPlanType);
                argument.putString("maxLimit", myTakaful.mMaxLimit);
                takafulTravelCardFragment.setArguments(argument);

                TakafulTravelCardFragment takafulTravelCardFragmentAr = new TakafulTravelCardFragment();
                Bundle argumentAR = new Bundle();
                argumentAR.putBoolean("ar", true);
                argumentAR.putString("name", myTakaful.mPolicyHolderName);
                argumentAR.putString("firstNumber", myTakaful.mPolicyNo);
                argumentAR.putString("finalNumber", " - " + myTakaful.mPassport);
                argumentAR.putString("policyStartDate", myTakaful.mPolicyStart);
                argumentAR.putString("policyEndDate", myTakaful.mPolicyEnd);
                argumentAR.putString("planType", myTakaful.mPlanType);
                argumentAR.putString("maxLimit", myTakaful.mMaxLimit);
                takafulTravelCardFragmentAr.setArguments(argumentAR);

                PagerAdapter mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
                mPagerAdapter.addFragment(takafulTravelCardFragment);
                mPagerAdapter.addFragment(takafulTravelCardFragmentAr);

                viewPager.setAdapter(mPagerAdapter);

                //to print family member if there is (only in travel policy)
                JSONArray memeberArray = resultObject.getJSONArray("Members");
                if (memeberArray.length() > 0) {
                    findViewById(R.id.lloMember).setVisibility(View.VISIBLE);
                    String membersText = "";
                    for (int i = 0; i < memeberArray.length(); i++) {
                        JSONObject memberObject = memeberArray.getJSONObject(i);
                        membersText += getString(R.string.name2) + memberObject.getString("Name") + "\n";
                        membersText += getString(R.string.passNo) + memberObject.getString("PassNo") + "\n";
                        membersText += "\n";
                    }
                    tvMembers.setText(membersText);
                }

                //set travel detail text
                JSONArray emergencyArray = resultObject.getJSONArray(getString(R.string.z_emergency_no));
                String emergencyNo = "";
                for (int i = 0; i < emergencyArray.length(); i++)
                    emergencyNo += emergencyArray.get(i) + "\n";
                findViewById(R.id.lloTravelDetail).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.tvEmergencyNumbers)).setText(emergencyNo);

            } else if (jsonObject.getInt("status") == 4) {
                MyMethods.showLoginPage(this);
            } else {
                MyMethods.showNetworkErrorDialog(this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getCard(APIs.GetTravelCardDetail);
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setFireBaitakCard(JSONObject jsonObject) {
        try {
            Log.i("setFireBaitakCard", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                JSONObject resultObject = jsonObject.getJSONObject("result");
                String cover = resultObject.getString("Cover");
                String sumCovered = resultObject.getString("SumCovered");
                String policyStart = resultObject.getString("PolicyStart");
                String policyEnd = resultObject.getString("PolicyEnd");

                String house = "", roadNo = "", blockNo = "", city = "", country = "";
                if (resultObject.has("PropertyAddress")) {
                    JSONObject addressObject = resultObject.getJSONObject("PropertyAddress");
                    house = addressObject.getString("houseNo");
                    roadNo = addressObject.getString("roadNo");
                    blockNo = addressObject.getString("blockNo");
                    city = addressObject.getString("city");
                    country = addressObject.getString("country");
                }

                myTakaful.addFireBaitakCardInfo(cover, sumCovered, policyStart, policyEnd, house,
                        roadNo, blockNo, city, country);

                TakafulFireBaitakCardFragment takafulFireBaitakCardFragment = new TakafulFireBaitakCardFragment();
                Bundle argument = new Bundle();
                argument.putString("name", myTakaful.mPolicyHolderName);
                argument.putString("firstNumber", myTakaful.mPolicyNo);
                argument.putString("finalNumber", " - " + myTakaful.mCpr);
                argument.putString("policyStartDate", myTakaful.mPolicyStart);
                argument.putString("policyEndDate", myTakaful.mPolicyEnd);
                argument.putString("cover", myTakaful.mCover);
                argument.putString("sumCover", myTakaful.mSumCover);
                takafulFireBaitakCardFragment.setArguments(argument);

                TakafulFireBaitakCardFragment takafulFireBaitakCardFragmentAr = new TakafulFireBaitakCardFragment();
                Bundle argumentAR = new Bundle();
                argumentAR.putBoolean("ar", true);
                argumentAR.putString("name", myTakaful.mPolicyHolderName);
                argumentAR.putString("firstNumber", myTakaful.mPolicyNo);
                argumentAR.putString("finalNumber", " - " + myTakaful.mCpr);
                argumentAR.putString("policyStartDate", myTakaful.mPolicyStart);
                argumentAR.putString("policyEndDate", myTakaful.mPolicyEnd);
                argumentAR.putString("cover", myTakaful.mCover);
                argumentAR.putString("sumCover", myTakaful.mSumCover);
                takafulFireBaitakCardFragmentAr.setArguments(argumentAR);

                PagerAdapter mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
                mPagerAdapter.addFragment(takafulFireBaitakCardFragment);
                mPagerAdapter.addFragment(takafulFireBaitakCardFragmentAr);

                viewPager.setAdapter(mPagerAdapter);
            } else if (jsonObject.getInt("status") == 4) {
                MyMethods.showLoginPage(this);
            } else {
                MyMethods.showNetworkErrorDialog(this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getCard(APIs.GetFireBaitakCardDetail);
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
