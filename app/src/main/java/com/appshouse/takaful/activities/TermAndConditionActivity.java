package com.appshouse.takaful.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import com.appshouse.takaful.R;
import com.appshouse.takaful.constant.ProductCodes;
import com.appshouse.takaful.utilities.MyMethods;

/**
 * Created by Mohammed Algassab on 5/3/2016.
 */
public class TermAndConditionActivity extends AppCompatActivity {
    Toolbar toolbar;
    CheckBox cbTermAccept;
    String productCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_condition);

        cbTermAccept = (CheckBox) findViewById(R.id.cbTermAccept);
        productCode = getIntent().getStringExtra("productCode");

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_term_condition);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.next, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.next:
                if (cbTermAccept.isChecked()) {
                    switch (productCode) {
                        case ProductCodes.MOTOR:
                            startActivity(new Intent(this, QuoteMotorActivity.class));
                            break;

                        case ProductCodes.TRAVEL:
                            startActivity(new Intent(this, QuoteTravelActivity.class));
                            break;

                        case ProductCodes.FIRE:
                            startActivity(new Intent(this, QuoteFireActivity.class));
                            break;

                        case ProductCodes.BAITAK:
                            startActivity(new Intent(this, QuoteBaitakActivity.class));
                            break;

                        default:
                            Toast.makeText(this, getString(R.string.not_available), Toast.LENGTH_LONG).show();
                            break;
                    }
                } else {
                    Snackbar.make(findViewById(R.id.main), R.string.message_accept_termCondition, Snackbar.LENGTH_INDEFINITE)
                            .setAction(getString(R.string.dialog_button_default), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                }
                            }).show();
                }
        }
        return super.onOptionsItemSelected(item);
    }
}

