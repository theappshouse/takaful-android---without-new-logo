package com.appshouse.takaful.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.adapters.SpinnerAdapter;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.constant.ProductCodes;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 5/3/2016.
 */
public class QuoteUserInfoActivity extends AppCompatActivity implements TextWatcher {
    Toolbar toolbar;
    String productCode;
    LinearLayout lloInputField;
    LinearLayout lloDob;
    Spinner sCity;
    EditText etCpr, etConfirmCpr, etFirstName, etLastName, etDob, etFlat, etBuilding, etRoad, etBlock,
            etPoBox, etMobile, etPhone, etEmail;
    RadioButton rdMale, rdFemale;
    View view;
    boolean isCreateNewClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote_user_info);
        view = findViewById(android.R.id.content);
        etCpr = (EditText) findViewById(R.id.etCpr);
        etConfirmCpr = (EditText) findViewById(R.id.etConfirmCpr);
        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etDob = (EditText) findViewById(R.id.etDob);
        etFlat = (EditText) findViewById(R.id.etFlat);
        etBuilding = (EditText) findViewById(R.id.etBuilding);
        etRoad = (EditText) findViewById(R.id.etRoad);
        etBlock = (EditText) findViewById(R.id.etBlock);
        etPoBox = (EditText) findViewById(R.id.etPoBox);
        etMobile = (EditText) findViewById(R.id.etMobile);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etEmail = (EditText) findViewById(R.id.etEmail);
        rdMale = (RadioButton) findViewById(R.id.rdMale);
        rdFemale = (RadioButton) findViewById(R.id.rdFemale);
        lloInputField = (LinearLayout) findViewById(R.id.lloInputField);
        lloDob = (LinearLayout) findViewById(R.id.lloDob);
        sCity = (Spinner) findViewById(R.id.sCity);
        productCode = getIntent().getStringExtra("productCode");

        mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.setCancelable(false);


        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.user_quotation);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        etCpr.addTextChangedListener(this);
        etConfirmCpr.addTextChangedListener(this);

        lloDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etDob.requestFocus();
                openPickDateDialog();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        setInputEnable(false);
        getCities();
    }


    private void openPickDateDialog() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String mYear, mMonth, mDay;
                mYear = String.valueOf(year);
                mMonth = String.valueOf(monthOfYear + 1);
                mDay = String.valueOf(dayOfMonth);

                String date = mDay + "/" + mMonth + "/" + mYear;
                etDob.setText(date);
            }
        }, year, month, day);
        datePickerDialog.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (etCpr.getText().length() == 9) {
            getQuoteUserInfo();
        }
    }

    String Tag_Request = "userInfo";
    ProgressDialog mDialog;

    private void getQuoteUserInfo() {
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetUserInfo
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                setQuoteUserInfo(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                MyMethods.showNetworkErrorDialog(QuoteUserInfoActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getQuoteUserInfo();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("cpr", etCpr.getText().toString());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    String oldMobileNo;

    private void setQuoteUserInfo(JSONObject response) {
        try {
            Log.i("setQuoteUserInfo", response.toString());
            if (response.getBoolean("success")) {
                JSONObject resultObject = response.getJSONObject("result");
                if (resultObject.getInt("errCode") == 0) {
                    Snackbar.make(findViewById(R.id.main), R.string.message_user_exist, Snackbar.LENGTH_LONG).show();
                    isCreateNewClient = false;
                    setInputEnable(false);
                    etFirstName.setText(resultObject.getString("firstName"));
                    etLastName.setText(resultObject.getString("lastName"));
                    etDob.setText(resultObject.getString("dob"));
                    etFlat.setText(resultObject.getString("flatNo"));
                    etBuilding.setText(resultObject.getString("houseNo"));
                    etRoad.setText(resultObject.getString("road"));
                    etBlock.setText(resultObject.getString("block"));
                    etPoBox.setText(resultObject.getString("postBoxNo"));
                    etMobile.setText(resultObject.getString("mobileNo"));
                    etPhone.setText(resultObject.getString("phoneNo"));
                    etEmail.setText(resultObject.getString("emailId"));

                    String gender = resultObject.getString("gender");
                    if (gender.contentEquals("M")) {
                        rdMale.setChecked(true);
                    } else {
                        rdFemale.setChecked(true);
                    }

                    String city = resultObject.getString("city");
                    for (int i = 0; i < cityArray.length; i++) {
                        if (city.contentEquals(cityArray[i])) {
                            sCity.setSelection(i);
                            break;
                        }
                    }

                    oldMobileNo = resultObject.getString("mobileNo");

                } else if (resultObject.getInt("errCode") == 1) {
                    Snackbar.make(findViewById(R.id.main), R.string.message_user_not_exist, Snackbar.LENGTH_LONG).show();
                    isCreateNewClient = true;
                    setInputEnable(true);
                }
            } else {
                MyMethods.showNetworkErrorDialog(QuoteUserInfoActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getQuoteUserInfo();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setInputEnable(boolean enable) {
        rdFemale.setEnabled(enable);
        rdMale.setEnabled(enable);
        lloDob.setEnabled(enable);
        for (int i = 0; i < lloInputField.getChildCount(); i++) {
            View child = lloInputField.getChildAt(i);
            child.setEnabled(enable);
        }
        etMobile.setEnabled(true);
    }

    String Tag_Request_City = "getCities";

    public void getCities() {
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.setCancelable(false);
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetCities
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                setSpinnerData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                MyMethods.showNetworkErrorDialog(QuoteUserInfoActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getCities();
                    }
                });
            }
        });
        AppController.getInstance().addToRequestQueue(req, Tag_Request_City);
    }

    String[] cityArray;

    private void setSpinnerData(JSONObject jsonObject) {
        try {
            if (jsonObject.getBoolean("success")) {
                JSONArray resultArray = jsonObject.getJSONArray("result");
                cityArray = new String[resultArray.length()];
                for (int i = 0; i < resultArray.length(); i++) {
                    cityArray[i] = resultArray.getJSONObject(i).getString("Name");
                }
                sCity.setAdapter(new SpinnerAdapter(this, cityArray));
            } else {
                MyMethods.showNetworkErrorDialog(QuoteUserInfoActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getCities();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.next, menu);
        return true;
    }

    String firstName, lastName, dob, flat, building, road, block, poBox, mobile,
            phone, email, cpr, gender, city;

    private void prepareCreateClient() {
        cpr = etCpr.getText().toString();
        firstName = etFirstName.getText().toString();
        lastName = etLastName.getText().toString();
        dob = etDob.getText().toString();
        flat = etFlat.getText().toString();
        building = etBuilding.getText().toString();
        road = etRoad.getText().toString();
        block = etBlock.getText().toString();
        poBox = etPoBox.getText().toString();
        mobile = etMobile.getText().toString();
        phone = etPhone.getText().toString();
        email = etEmail.getText().toString();

        if (rdMale.isChecked())
            gender = "M";
        else if (rdFemale.isChecked())
            gender = "F";
        else
            gender = "";

        city = cityArray[sCity.getSelectedItemPosition()];

        if (cpr.isEmpty() || firstName.isEmpty() || lastName.isEmpty() || dob.isEmpty()
                || gender.isEmpty() || email.isEmpty() || cpr.isEmpty()) {
            Snackbar.make(findViewById(R.id.main), R.string.message_enterRequiredData, Snackbar.LENGTH_LONG).show();
        } else {
            createNewClient();
        }
    }

    String Tag_Request_CreateUser = "createNewClinet";

    private void createNewClient() {
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.CreateNewClient
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                setCreateClientResult(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Snackbar.make(findViewById(R.id.main), R.string.networkError, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                createNewClient();
                            }
                        }).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                Log.i("cpr", cpr);
                Log.i("firstName", firstName);
                Log.i("lastName", lastName);
                Log.i("dob", dob);
                Log.i("flat", flat);
                Log.i("building", building);
                Log.i("road", road);
                Log.i("block", block);
                Log.i("poBox", poBox);
                Log.i("mobile", mobile);
                Log.i("phone", phone);
                Log.i("email", email);
                Log.i("gender", gender);
                Log.i("city", city);

                params.put("cpr", cpr);
                params.put("firstName", firstName);
                params.put("lastName", lastName);
                params.put("dob", dob);
                params.put("flat", flat);
                params.put("building", building);
                params.put("road", road);
                params.put("block", block);
                params.put("poBox", poBox);
                params.put("mobile", mobile);
                params.put("phone", phone);
                params.put("email", email);
                params.put("gender", gender);
                params.put("city", city);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request_CreateUser);
    }

    private void setCreateClientResult(JSONObject response) {
        try {
            Log.i("setCreateClientResult", response.toString());
            if (response.getBoolean("success")) {
                goToGetQuotePage();
            } else {
                Snackbar.make(findViewById(R.id.main), response.getString(getString(R.string.api_message)), Snackbar.LENGTH_INDEFINITE).
                        setAction(R.string.dialog_button_default, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        }).show();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void goToGetQuotePage() {
        Intent intent = null;
        switch (productCode) {
            case ProductCodes.MOTOR:
                intent = new Intent(this, QuoteMotorActivity.class);
                break;

            case ProductCodes.TRAVEL:
                intent = new Intent(this, QuoteTravelActivity.class);
                break;

            case ProductCodes.FIRE:
                intent = new Intent(this, QuoteFireActivity.class);
                break;

            case ProductCodes.BAITAK:
                intent = new Intent(this, QuoteBaitakActivity.class);
                break;

            default:
                Toast.makeText(this, getString(R.string.not_available), Toast.LENGTH_LONG).show();
                break;
        }

        intent.putExtra("cpr", cpr);
        startActivity(intent);
    }

    private void updateMobileNo() {
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.UpdateMobile
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                handleUpdateMobileNo(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Snackbar.make(findViewById(R.id.main), R.string.networkError, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                updateMobileNo();
                            }
                        }).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SharedPreferencesClass setting =  new SharedPreferencesClass(QuoteUserInfoActivity.this);

                params.put("cpr", cpr);
                params.put("mobileNo", updatedMobile);
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("deviceId", MyMethods.getDeviceId(QuoteUserInfoActivity.this));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request_CreateUser);
    }

    private void handleUpdateMobileNo(JSONObject response){
        try {
            if (response.getBoolean("success")) {
                goToGetQuotePage();
            } else {
                MyMethods.showSnackBarMessage(findViewById(R.id.main), response.getString(getString(R.string.api_message)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    String updatedMobile;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.next:
                MyMethods.hideKeyboard(this);
                if (isCreateNewClient) {
                    prepareCreateClient();
                } else {
                    cpr = etCpr.getText().toString();
                    if (cpr.length() == 9) {
                        if (oldMobileNo.contentEquals(etMobile.getText().toString()))
                            goToGetQuotePage();
                        else {
                            updatedMobile = etMobile.getText().toString();
                            if (!updatedMobile.isEmpty())
                                updateMobileNo();
                            else
                                Snackbar.make(findViewById(R.id.main), R.string.message_enter_mobileNo, Snackbar.LENGTH_LONG);
                        }
                    } else {
                        Snackbar.make(findViewById(R.id.main), R.string.message_enter_cpr, Snackbar.LENGTH_INDEFINITE).
                                setAction(R.string.dialog_button_default, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                }).show();
                    }
                }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (etConfirmCpr.getText().length() == 9 && etCpr.getText().length() == 9) {
            MyMethods.hideKeyboard(this);
            if (etCpr.getText().toString().contains(etConfirmCpr.getText().toString()) && !mDialog.isShowing()) {
                getQuoteUserInfo();
            } else {
                Snackbar.make(findViewById(R.id.main), R.string.message_cpr_not_match, Snackbar.LENGTH_LONG).show();
            }
        }
    }
}

