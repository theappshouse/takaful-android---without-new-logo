package com.appshouse.takaful.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 2/9/2016.
 */
public class ForgetPasswordActivity extends AppCompatActivity {
    Toolbar toolbar;
    LinearLayout lloSubmitEmail, lloSubmitPassword;
    EditText etEmail, etPassword, etPasswordConfirm, etCode;
    Button bSubmit;
    int userId;
    SharedPreferencesClass setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forget_password);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.reset_password);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setting = new SharedPreferencesClass(this);

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etPasswordConfirm = (EditText) findViewById(R.id.etPasswordConfirm);
        etCode = (EditText) findViewById(R.id.etCode);

        lloSubmitEmail = (LinearLayout) findViewById(R.id.lloSubmitEmail);
        lloSubmitPassword = (LinearLayout) findViewById(R.id.lloSubmitPassword);

        etPassword.setGravity(setting.getLanguage().contentEquals("en") ? Gravity.LEFT : Gravity.RIGHT);
        etPasswordConfirm.setGravity(setting.getLanguage().contentEquals("en") ? Gravity.LEFT : Gravity.RIGHT);
        bSubmit = (Button) findViewById(R.id.bSubmit);
        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //If lloSubmitEmail is visible we know we did not send request for resting the password
                if (lloSubmitEmail.getVisibility() == View.VISIBLE) {
                    sendCodeToEmail();
                } else {
                    //We sent the request now we are trying to reset the password using the code received  in email
                    if (etPassword.getText().toString().contentEquals(etPasswordConfirm.getText().toString()))
                        resetPassword();
                    else
                        MyMethods.showSnackBarMessage(findViewById(R.id.main), getString(R.string.message_passwordNotMatched));
                }
            }
        });
    }

    String Tag_Request_SendEmail = "sendEmail";

    //request a code to reset the password
    private void sendCodeToEmail() {
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.SendCodeToEmail
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                getSendCodeToEmailResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Snackbar.make(findViewById(R.id.main), R.string.networkError, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                sendCodeToEmail();
                            }
                        }).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", etEmail.getText().toString());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request_SendEmail);
    }

    private void getSendCodeToEmailResult(JSONObject jsonObject) {
        try {
            Log.i("getSendEmailResult", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                                userId = jsonObject.getInt("result");
                Snackbar.make(findViewById(R.id.main), R.string.message_confirmationCodeSent, Snackbar.LENGTH_INDEFINITE).
                        setAction(R.string.dialog_button_default, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        }).show();
                lloSubmitEmail.setVisibility(View.GONE);
                lloSubmitPassword.setVisibility(View.VISIBLE);
            } else {
                MyMethods.showSnackBarMessage(findViewById(R.id.main), jsonObject.getString(getString(R.string.api_message)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    String Tag_Request_SendPassword = "sendPassword";

    //Rset the password using the code received from email
    private void resetPassword() {
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.ResetPassword
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                getResetPasswordResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Snackbar.make(findViewById(R.id.main), R.string.networkError, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                resetPassword();
                            }
                        }).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("password", etPassword.getText().toString());
                params.put("code", etCode.getText().toString());
                params.put("userId", String.valueOf(userId));
                params.put("deviceId", MyMethods.getDeviceId(ForgetPasswordActivity.this));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request_SendPassword);
    }

    private void getResetPasswordResult(JSONObject jsonObject) {
        try {
            Log.i("getResetPasswordResult", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                MyMethods.showGeneralDialogMessage(this,
                        R.string.message_passwordResetSuccess, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
            } else {
                MyMethods.showSnackBarMessage(findViewById(R.id.main), jsonObject.getString(getString(R.string.api_message)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
