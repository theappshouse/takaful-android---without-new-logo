package com.appshouse.takaful.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 2/8/2016.
 */
public class WebViewPaymentActivity extends AppCompatActivity {
    WebView wbPreview;
    View view;
    Toolbar toolbar;
    String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_payement);
        view = findViewById(android.R.id.content);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.payment);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        wbPreview = (WebView) findViewById(R.id.wbPreview);
        wbPreview.getSettings().setLoadWithOverviewMode(true);
        wbPreview.getSettings().setJavaScriptEnabled(true);
        wbPreview.setWebViewClient(new MyWebViewClient());
        wbPreview.addJavascriptInterface(new PaymentInterface(this), "Payment");
        wbPreview.getSettings().setDomStorageEnabled(true);

        //link = "https://mob.takafulweb.com/paymentMethod.php?payment=DebitCard&payFor=new&payFor_id=Q/1799GATT020735&platform=Android&cpr=191919191";
        link = getIntent().getStringExtra("link");
        Log.i("ViewLink:" , link);
        loadUrl();

    }

    public class PaymentInterface {

        Context mContext;

        PaymentInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void onResult(String response) {

            handleResult(response);

        }
    }

    public void handleResult(String response) {

        try {
            Log.i("response", response);
            final JSONObject jObject = new JSONObject(response);
            String message = jObject.getString(getString(R.string.api_message));

            if (jObject.getBoolean("success")) {

                AlertDialog.Builder dialog = new AlertDialog.Builder(WebViewPaymentActivity.this);
                dialog.setTitle(R.string.transaction_success);
                dialog.setMessage(message);
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(WebViewPaymentActivity.this, MainActivity.class);
                        i.putExtra("isMyTakafulPage", true);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                    }
                });
                dialog.setCancelable(false);
                dialog.show();
            } else {
                final int status = jObject.getInt("status");
                AlertDialog.Builder dialog = new AlertDialog.Builder(WebViewPaymentActivity.this);
                dialog.setTitle(R.string.failed);

                dialog.setMessage(message);

                if (status == 3) {
                    amount = jObject.optString("paymentAmount");
                    paymentId = jObject.optString("paymentId");
                    if (getIntent().getStringExtra("payFor").contentEquals("renewal")) {
                        date = jObject.optString("date");
                    }
                }

                dialog.setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (status == 3) {
                            WebViewPaymentActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    issueRenewQuote();
                                }
                            });
                        } else {
                            loadUrl();
                        }
                    }
                });
                dialog.setNeutralButton(R.string.back, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                dialog.setCancelable(false);
                if (!isFinishing())
                    dialog.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    String Tag_Request = "issueRenew";
    String amount, paymentId, date = "";

    private void issueRenewQuote() {

        MyMethods.showLoading(view);

        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.IssueRenewQuote
                , null, new Response.Listener<JSONObject>() {



            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                handleResult(response.toString());


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showNetworkErrorDialog(WebViewPaymentActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        issueRenewQuote();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SharedPreferencesClass setting = new SharedPreferencesClass(WebViewPaymentActivity.this);
                params.put("no", getIntent().getStringExtra("no"));
                params.put("payFor", getIntent().getStringExtra("payFor"));
                params.put("amount", amount);
                params.put("paymentId", paymentId);
                params.put("date", date);
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("cpr", setting.getCpr());
                params.put("deviceId", MyMethods.getDeviceId(WebViewPaymentActivity.this));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    //this also will be called from Receiver class
    public void loadUrl() {
        wbPreview.post(new Runnable() {
            @Override
            public void run() {
                wbPreview.loadUrl(link);


            }
        });
    }

    private class MyWebViewClient extends WebViewClient {
        boolean successLoad = true;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            MyMethods.showLoading(WebViewPaymentActivity.this.view);
            successLoad = true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (successLoad) {
                MyMethods.showMainView(WebViewPaymentActivity.this.view);
                view.clearCache(true);
            }
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, final String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            successLoad = false;
            MyMethods.showNetworkErrorDialog(WebViewPaymentActivity.this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    wbPreview.loadUrl(failingUrl);
                }
            });
        }


        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
            String message = "SSL Certificate error.";
            switch (error.getPrimaryError()) {
                case SslError.SSL_UNTRUSTED:
                    message = "There is error in trusting this certificate authority.";
                    break;
                case SslError.SSL_EXPIRED:
                    message = "The certificate has expired.";
                    break;
                case SslError.SSL_IDMISMATCH:
                    message = "The certificate Hostname mismatch.";
                    break;
                case SslError.SSL_NOTYETVALID:
                    message = "The certificate is not yet valid.";
                    break;
            }
            message += " Do you want to continue?";

            builder.setTitle("SSL Certificate Error");
            builder.setMessage(message);
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();

                    //or open the link in phone web browser
//                    MyMethods.openLink(WebViewPaymentActivity.this,link);
//                    finish();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

}
