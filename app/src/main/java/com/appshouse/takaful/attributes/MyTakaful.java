package com.appshouse.takaful.attributes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohammed Algassab on 1/6/2016.
 */
public class MyTakaful implements Parcelable {


    public boolean mIsRenew;
    public boolean mIsExpired;
    public int mStatus;
    public String mProductCode;
    public String mStartDate;
    public String mExpireDate;
    public String mPolicyNo;
    public String mQuotationNo;
    public String mPolicyHolderName;
    public String mPolicyInceptionDate;
    public String mPolicyGrossPremium;
    public String mPolicyNetPremium;
    public String mPolicySumInsured;
    public String mProductName;


    public MyTakaful(String quotationNo, String policyNo, boolean isRenew, boolean isExpired, int status, String productCode,
                     String startDate, String expireDate, String policyHolderName, String policyInceptionDate,
                     String policyGrossPremium, String policyNetPremium, String policySumInsured, String productName) {
        mQuotationNo = quotationNo;
        mPolicyNo = policyNo;
        mIsRenew = isRenew;
        mIsExpired = isExpired;
        mStatus = status;
        mProductCode = productCode;
        mStartDate = startDate;
        mExpireDate = expireDate;
        mPolicyHolderName = policyHolderName;
        mPolicyInceptionDate = policyInceptionDate;
        mPolicyGrossPremium = policyGrossPremium;
        mPolicyNetPremium = policyNetPremium;
        mPolicySumInsured = policySumInsured;
        mProductName = productName;
    }


    public String mCpr;
    public String mCommencement;
    public String mCover;
    public String mExpiryDate;
    public String mMakeAndModel;
    public String mRegNumber;
    public String mExcessAmt;
    public String mDateOfIssue;
    public String mAddlBenefits;

    public void addMotorCardInfo(String cpr, String commencement, String cover, String expiryDate, String makeAndModel,
                                 String regNumber, String excessAmt, String dateOfIssue, String addlBenefits) {
        mCpr = cpr;
        mCommencement = commencement;
        mCover = cover;
        mExpiryDate = expiryDate;
        mMakeAndModel = makeAndModel;
        mRegNumber = regNumber;
        mExcessAmt = excessAmt;
        mDateOfIssue = dateOfIssue;
        mAddlBenefits = addlBenefits;
    }


    public String mPlanType;
    public String mMaxLimit;
    public String mPolicyStart;
    public String mPolicyEnd;
    public String mPassport;

    public void addTravelCardInfo(String planType, String maxLimit, String policyStart,
                                  String policyEnd, String passport) {
        mPlanType = planType;
        mMaxLimit = maxLimit;
        mPolicyStart = policyStart;
        mPolicyEnd = policyEnd;
        mPassport = passport;
    }

    public String mSumCover;
    public String mHouseNo;
    public String mRoadNo;
    public String mBlockNo;
    public String mCity;
    public String mCountry;

    public void addFireBaitakCardInfo(String cover, String sumCovered, String policyStart, String policyEnd,
                                      String house, String roadNo, String blockNo, String city, String country) {
        mCover = cover;
        mSumCover = sumCovered;
        mPolicyStart = policyStart;
        mPolicyEnd = policyEnd;
        mHouseNo = house;
        mRoadNo = roadNo;
        mBlockNo = blockNo;
        mCity = city;
        mCountry = country;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public MyTakaful(Parcel in) {
        readFromParcel(in);
    }

    private void readFromParcel(Parcel in) {
        mIsRenew = in.readByte() != 0;
        mIsExpired = in.readByte() != 0;
        mStatus = in.readInt();
        mProductCode = in.readString();
        mStartDate = in.readString();
        mExpireDate = in.readString();
        mPolicyNo = in.readString();
        mQuotationNo = in.readString();
        mPolicyHolderName = in.readString();
        mPolicyInceptionDate = in.readString();
        mPolicyGrossPremium = in.readString();
        mPolicyNetPremium = in.readString();
        mPolicySumInsured = in.readString();
        mProductName = in.readString();

        //MotorCard
        mCpr = in.readString();
        mCommencement = in.readString();
        mCover = in.readString();
        mExpiryDate = in.readString();
        mMakeAndModel = in.readString();
        mRegNumber = in.readString();
        mExcessAmt = in.readString();
        mDateOfIssue = in.readString();
        mAddlBenefits = in.readString();

        //TravelCard
        mPlanType = in.readString();
        mMaxLimit = in.readString();
        mPolicyStart = in.readString();
        mPolicyEnd = in.readString();
        mPassport = in.readString();

        //FireBaitakCard
        mSumCover = in.readString();
        mHouseNo = in.readString();
        mRoadNo = in.readString();
        mBlockNo = in.readString();
        mCity = in.readString();
        mCountry = in.readString();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeByte((byte) (mIsRenew ? 1 : 0));
        out.writeByte((byte) (mIsExpired ? 1 : 0));
        out.writeInt(mStatus);
        out.writeString(mProductCode);
        out.writeString(mStartDate);
        out.writeString(mExpireDate);
        out.writeString(mPolicyNo);
        out.writeString(mQuotationNo);
        out.writeString(mPolicyHolderName);
        out.writeString(mPolicyInceptionDate);
        out.writeString(mPolicyGrossPremium);
        out.writeString(mPolicyNetPremium);
        out.writeString(mPolicySumInsured);
        out.writeString(mProductName);

        //MotorCard
        out.writeString(mCpr);
        out.writeString(mCommencement);
        out.writeString(mCover);
        out.writeString(mExpiryDate);
        out.writeString(mMakeAndModel);
        out.writeString(mRegNumber);
        out.writeString(mExcessAmt);
        out.writeString(mDateOfIssue);
        out.writeString(mAddlBenefits);

        //TravelCard
        out.writeString(mPlanType);
        out.writeString(mMaxLimit);
        out.writeString(mPolicyStart);
        out.writeString(mPolicyEnd);
        out.writeString(mPassport);

        //FireBaitakCard
        out.writeString(mSumCover);
        out.writeString(mHouseNo);
        out.writeString(mRoadNo);
        out.writeString(mBlockNo);
        out.writeString(mCity);
        out.writeString(mCountry);
    }

    public static final Parcelable.Creator<MyTakaful> CREATOR = new Parcelable.Creator<MyTakaful>() {
        public MyTakaful createFromParcel(Parcel in) {
            return new MyTakaful(in);
        }

        public MyTakaful[] newArray(int size) {
            return new MyTakaful[size];
        }
    };
}
