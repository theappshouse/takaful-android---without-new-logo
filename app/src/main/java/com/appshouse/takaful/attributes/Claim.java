package com.appshouse.takaful.attributes;

import org.json.JSONObject;

/**
 * Created by Mohammed Algassab on 1/13/2016.
 */
public class Claim {

    public String mNo;
    public int mStatus;
    public JSONObject mClaimDetailObject;
    public String mReasonRejection;

    public Claim(String no, int status, JSONObject claimDetailObject,String reasonRejection) {
        mNo = no;
        mStatus = status;
        mClaimDetailObject = claimDetailObject;
        mReasonRejection = reasonRejection;
    }


}
