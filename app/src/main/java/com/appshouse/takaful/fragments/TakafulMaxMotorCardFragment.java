package com.appshouse.takaful.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appshouse.takaful.R;

/**
 * Created by Mohammed Algassab on 1/9/2016.
 */
public class TakafulMaxMotorCardFragment extends Fragment {

    TextView tvName, tvFirstNumber, tvFinalNumber, tvCommencementDateTime, tvRegNo,
            tvExpiryDateTime, tvExcess, tvVehicleYearType, tvCover, tvDateOfIssue, tvADDl;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_pager_max_motor_card, container, false);
        tvName = (TextView) view.findViewById(R.id.tvName);
        tvFirstNumber = (TextView) view.findViewById(R.id.tvFirstNumber);
        tvFinalNumber = (TextView) view.findViewById(R.id.tvFinalNumber);
        tvCommencementDateTime = (TextView) view.findViewById(R.id.tvCommencementDateTime);
        tvExpiryDateTime = (TextView) view.findViewById(R.id.tvExpiryDateTime);
        tvExcess = (TextView) view.findViewById(R.id.tvExcess);
        tvVehicleYearType = (TextView) view.findViewById(R.id.tvVehicleYearType);
        tvDateOfIssue = (TextView) view.findViewById(R.id.tvDateOfIssue);
        tvADDl = (TextView) view.findViewById(R.id.tvADDL);
        tvCover = (TextView) view.findViewById(R.id.tvCover);
        tvRegNo = (TextView) view.findViewById(R.id.tvRegNo);


        Bundle bundle = getArguments();
        if (bundle.getBoolean("ar")) {
            ((TextView) view.findViewById(R.id.tvCommencementTitle)).setText("إبتداء التأمين");
            ((TextView) view.findViewById(R.id.tvExpiryTitle)).setText("إنتهاء التأمين");
            ((TextView) view.findViewById(R.id.tvVehicleTitle)).setText("السيارة المغطاة");
            ((TextView) view.findViewById(R.id.tvCoverTitle)).setText("نوع الغطاء");
            ((TextView) view.findViewById(R.id.tvExcessTitle)).setText("مبلغ التحمل");
            ((TextView) view.findViewById(R.id.tvDateOfIssueTitle)).setText("تاريخ الإصدار");
            ((TextView) view.findViewById(R.id.tvRegNoTitle)).setText("رقم التسجيل/القاعدة");
            ((TextView) view.findViewById(R.id.tvADDLTitle)).setText("ميزات أخرى");
        }
        tvName.setText(bundle.getString("name"));
        tvFirstNumber.setText(bundle.getString("firstNumber"));
        tvFinalNumber.setText(bundle.getString("finalNumber"));
        tvCommencementDateTime.setText(bundle.getString("commencementDateTime"));
        tvExpiryDateTime.setText(bundle.getString("expiryDateTime"));
        tvExcess.setText(bundle.getString("excess"));
        tvDateOfIssue.setText(bundle.getString("dateOfIssue"));
        tvVehicleYearType.setText(bundle.getString("vehicleYearType"));
        tvRegNo.setText(bundle.getString("regNo"));
        tvCover.setText(bundle.getString("cover"));
        tvADDl.setText(bundle.getString("addl"));

        return view;
    }
}
