package com.appshouse.takaful.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.appshouse.takaful.activities.MainActivity;
import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.MyNoteActivity;
import com.appshouse.takaful.adapters.SideMenuAdapter;
import com.appshouse.takaful.attributes.SideMenu;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

/**
 * Created by Mohammed Algassab on 12/28/2015.
 */
public class SideMenuFragment extends Fragment implements AdapterView.OnItemClickListener {
    int home, takaful, claim, products, login, getQuote, roadAssistance,
            support, directory, ourCenter, myNote,news,language;
    LinearLayout lloAppsHouse;
    MainActivity mainActivity;
    SharedPreferencesClass setting;
    SideMenuAdapter sideMenuAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.side_menu, container, false);
        ListView listView = (ListView) view.findViewById(R.id.lvSideMenu);

        setting = new SharedPreferencesClass(getActivity());
        lloAppsHouse = (LinearLayout) view.findViewById(R.id.lloAppsHouse);
        lloAppsHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMethods.openLink(getActivity(), "http://theappshouse.com/");
            }
        });

        mainActivity = (MainActivity) getActivity();
        setSlideMenuItem();
        listView.setAdapter(sideMenuAdapter);
        listView.setOnItemClickListener(this);
        return view;
    }

    private void setSlideMenuItem() {
        sideMenuAdapter = new SideMenuAdapter(getActivity());

        sideMenuAdapter.add(new SideMenu(R.drawable.sm_language, getString(R.string.sm_language)));
        language = sideMenuAdapter.getCount();

        sideMenuAdapter.add(new SideMenu(R.drawable.home, getString(R.string.sm_home)));
        home = sideMenuAdapter.getCount();
        sideMenuAdapter.add(new SideMenu(R.drawable.login, setting.isLogin() ?
                getString(R.string.sm_userInfo) : getString(R.string.sm_login)));
        login = sideMenuAdapter.getCount();
        sideMenuAdapter.add(new SideMenu(R.drawable.sm_news, getString(R.string.sm_news)));
        news  = sideMenuAdapter.getCount();
        if (setting.isLogin()) {
            sideMenuAdapter.add(new SideMenu(R.drawable.my_takaful, getString(R.string.sm_myTakaful)));
            takaful = sideMenuAdapter.getCount();
            sideMenuAdapter.add(new SideMenu(R.drawable.my_claims, getString(R.string.sm_myClaim)));
            claim = sideMenuAdapter.getCount();
            sideMenuAdapter.add(new SideMenu(R.drawable.get_quote, getString(R.string.sm_getQuote)));
            getQuote = sideMenuAdapter.getCount();
            sideMenuAdapter.add(new SideMenu(R.drawable.road_assistant, getString(R.string.sm_roadAssistance)));
            roadAssistance = sideMenuAdapter.getCount();
        }
        sideMenuAdapter.add(new SideMenu(R.drawable.sm_products, getString(R.string.sm_product)));
        products = sideMenuAdapter.getCount();
        sideMenuAdapter.add(new SideMenu(R.drawable.our_centers, getString(R.string.sm_ourCenter)));
        ourCenter = sideMenuAdapter.getCount();
        sideMenuAdapter.add(new SideMenu(R.drawable.support, getString(R.string.sm_support)));
        support = sideMenuAdapter.getCount();
        sideMenuAdapter.add(new SideMenu(R.drawable.directory, getString(R.string.sm_directory)));
        directory = sideMenuAdapter.getCount();
        sideMenuAdapter.add(new SideMenu(R.drawable.my_note, getString(R.string.sm_myNote)));
        myNote = sideMenuAdapter.getCount();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        position++;
        Log.i("position"," selected:" + position);
        mainActivity.closeDrawerLayout();
        if (position == home) {
            mainActivity.replaceFragment(new HomeFragment(), getString(R.string.sm_home), true);
        } else if (position == takaful) {
            mainActivity.replaceFragment(new MyTakafulFragment(), getString(R.string.sm_myTakaful), false);
        } else if (position == claim) {
            mainActivity.replaceFragment(new ClaimListFragment(), getString(R.string.sm_myClaim), false);
        } else if (position == products) {
            mainActivity.replaceFragment(new ProductFragment(), getString(R.string.sm_product), false);
        } else if (position == login) {
            mainActivity.replaceFragment(setting.isLogin() ? new UserInfoFragment() : new LoginFragment(),
                    setting.isLogin() ? getString(R.string.sm_userInfo) : getString(R.string.sm_login), false);
        } else if (position == getQuote) {
            mainActivity.replaceFragment(new GetQuoteFragment(), getString(R.string.sm_getQuote), false);
        } else if (position == roadAssistance) {
            mainActivity.replaceFragment(new RoadAssistanceFragment(), getString(R.string.sm_roadAssistance), false);
        } else if (position == support) {
            mainActivity.replaceFragment(new SupportFragment(), getString(R.string.sm_support), false);
        } else if (position == ourCenter) {
            mainActivity.replaceFragment(new OurCenterFragment(), getString(R.string.sm_ourCenter), false);
        } else if (position == directory) {
            mainActivity.replaceFragment(new DirectoryFragment(), getString(R.string.sm_directory), false);
        } else if (position == myNote) {
            mainActivity.startActivity(new Intent(getActivity(), MyNoteActivity.class));
        }else if(position == news){
            mainActivity.replaceFragment(new NewsFragment(), getString(R.string.sm_news), false);
        }else if (position == language){
            setting.changeLanguage(getActivity(),setting.getLanguage().contentEquals("en") ? "ar" : "en");
        }
    }
}
