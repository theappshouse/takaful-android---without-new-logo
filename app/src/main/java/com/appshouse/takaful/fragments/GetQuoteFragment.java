package com.appshouse.takaful.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.AppController;
import com.appshouse.takaful.adapters.QuoteProductAdapter;
import com.appshouse.takaful.attributes.Product;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 1/31/2016.
 */
public class GetQuoteFragment extends Fragment {
    View view;
    RecyclerView lvProduct;
    ArrayList<Product> productArrayList;
    RecyclerView.Adapter productAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_get_quote, container, false);

        lvProduct = (RecyclerView) view.findViewById(R.id.lvProduct);
        lvProduct.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        lvProduct.setHasFixedSize(true);

        ((TextView) view.findViewById(R.id.tvTitle)).setTypeface(MyMethods.getRegularFont(getActivity()));

        getProductQuote();

        /*final AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
        ad.setCancelable(false);
        //ad.setMessage("We are upgrading our system to provide you with better services. Apologies for any inconvenient this might cause.");
        ad.setMessage(getString(R.string.vatmessage));
        ad.setButton(Dialog.BUTTON_NEUTRAL,"OK",new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        ad.show();*/
        return view;
    }

    String Tag_Request = "get_product_quote";

    private void getProductQuote() {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetProducts
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                setProductQuote(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getProductQuote();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("hasQuote", "1");

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setProductQuote(JSONObject jsonObject) {
        try {
            Log.i("setProductQuote", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                productArrayList = new ArrayList<>();
                JSONArray resultArray = jsonObject.getJSONArray("result");

                for (int i = 0; i < resultArray.length(); i++) {
                    JSONObject productObject = resultArray.getJSONObject(i);
                    Product product = new Product(productObject.getInt("Id"), productObject.getString("Product_Code"),
                            productObject.getString(getString(R.string.z_product_name)), productObject.getString(getString(R.string.z_short_description)),
                            productObject.getString(getString(R.string.z_product_details)), productObject.getString("Product_Image"),
                            productObject.getString("Product_Thumbnail"), productObject.getInt("Is_Feature") == 1,
                            productObject.getInt("Has_Quote") == 1);
                    productArrayList.add(product);
                }

                productAdapter = new QuoteProductAdapter(getActivity(), productArrayList);
                lvProduct.setAdapter(productAdapter);
            } else {
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getProductQuote();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        AppController.getInstance().cancelPendingRequests(Tag_Request);
    }
}
