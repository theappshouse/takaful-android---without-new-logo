package com.appshouse.takaful.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appshouse.takaful.R;
import com.appshouse.takaful.adapters.DirectoryAdapter;
import com.appshouse.takaful.attributes.Directory;
import com.appshouse.takaful.constant.APIs;

import java.util.ArrayList;

/**
 * Created by Mohammed Algassab on 5/12/2016.
 */
public class DirectoryFragment extends Fragment {

    RecyclerView lvDirectory;
    ArrayList<Directory> directoryArrayList;
    RecyclerView.Adapter directoryAdapter;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_directory, container, false);

        lvDirectory = (RecyclerView) view.findViewById(R.id.lvDirectory);
        lvDirectory.setLayoutManager(new LinearLayoutManager(getActivity()));
        lvDirectory.setHasFixedSize(true);

        setDirectory();
        return view;
    }

    private void setDirectory() {
        directoryArrayList = new ArrayList<>();
        directoryArrayList.add(new Directory(getString(R.string.agency_motor), R.drawable.dr_agency, ""));
//        directoryArrayList.add(new Directory("Vehicle Registration Renewal", R.drawable.dr_registraion, "http://www.bahrain.bh/wps/portal/!ut/p/a1/rZJbT8JAEIX_Cj7w2Ox0t5TlsSUCVpsaQKB9IUs7hVV6oaz18utd0MSoAWvivk3y7cw5Z4ZEZEGiXNRyLZQscrE91JG9HAVgm5RTD6YdF5wgoNfU8xj3LA2EGoATz4Gv_4PJgIPDZ_7Q7Q0YDBmZk4hEca5KtSEhrou6LColtktRtWFTZNiGFIV6rDBp4R6rWsa4b0ONGxlvsVXhWu5VdVSrixyftGbdsIxlQkJKTRMsSIwVW3HD4mnH6PGUG7GwLJpgJ-U0-TBwRmGjAD4Bxi41YHdvu7MetTk0DJD6ffPK0oDv69i4O76ZDvrQYP4xwDMdOP0FOIx4B84s8ZuKHyZD7aJ7UqZLyeSPa_Ea5Cbvd7vI0edT5AqfFVn8z_2U2V3G2YuUxsN49DpNs7lz8QZ0R2Z1/dl5/d5/L0lDUmlTUSEhL3dHa0FKRnNBLzRKVXBDQSEhL2Vu/"));
//        directoryArrayList.add(new Directory("Payment Of Traffic Contraventions", R.drawable.dr_traffic, "https://services.bahrain.bh/wps/portal/!ut/p/a1/jc_BDsIgEATQT2KgtMUjpaYgGmIMWrmYHowhUfRgjJ9v7V3s3jZ5k50lgfQkpOEVL8Mz3tNw_e6hOgnRKcoEW3V0LyCtg17ygkJVIziOoBRMaTsCV_ASkttt450u4DAvrzqpeb0GwAWDaRvd1osNYKp5efwY-ff-7pzIgYSJ5b6YQK7mBDI9Hjfv-7eJH8bmCgE!/dl5/d5/L2dBISEvZ0FBIS9nQSEh/"));
//        directoryArrayList.add(new Directory("Vehicles Record Enquiry", R.drawable.dr_vehicle, "https://services.bahrain.bh/wps/portal/!ut/p/a1/jZBNb4JAEIZ_iweuzAALbntb0bCIDWkaFPdilgSBhgJZt-LPd8utSYvO7Z08T-YDBOQgOnltKqmbvpPtTxbBidIodFzqbiNnT5ElKfIN8RwMAwMcDeBTN-SJAVKP-MhI8r7KUu5his_5YcQ4We4QkVAX4_WKr5cvb4hx8JyP_xR7OP9DKjiAmLC5KyZgbs0JmNljC6Jq-2L66ZF1hUcrEKo8l6pU9rcy7Vrr4dVCC8dxtAtZK9l0dlGbPFwsHHql_5br_qIh_y3B8JVl-S3-9NvrjrHF4g4x7HLq/dl5/d5/L0lHSkovd0RNQUprQUVnQSEhLzRKU0UvZW4"));
//        directoryArrayList.add(new Directory("Driving License Renewal", R.drawable.dr_driving, "http://www.bahrain.bh/wps/portal/!ut/p/a1/pZJRb4IwFIX_invwkbQUVsojGHVDkWU6J30xFa5YgwWBsP38Vbdk2RYdy_p2k6_3nnPuRRytEFeilZloZKFEfqo5Xd9FmJqEkQAvbn3sRRGZkCCwWGBrINYAvvA8_PV_NB8x7LFlOPbdkYXHFnpGHPFENWWzQzFkRVsWVSPytaj6eFccoI9FnveghqqVCdR9nFaylSrr5bpUNfQqUPCileo2pcgghVpm6lwlMkUx2wCYJnENe2M7hm0zagiKU8NhwoEkZZZrmx8mrqjsFMInYFlDDVDnwVm6hDLcMUQSDsx7WwNhqKNj_uN0MRrgDvPPIV7pwMgvwGnEO3Blkd9U_DAZaxfORZk-QfM_riXokJvcH4_c0ydUqAZeG7T6zw2Vh6cD228nkyl9HG5nM4PHN2_GT6vR/dl5/d5/L0lDUmlTUSEhL3dHa0FKRnNBLzRKVXBDQSEhL2Vu/"));
        directoryArrayList.add(new Directory(getString(R.string.agency_vchicle), R.drawable.dr_registraion, getString(R.string.dir_link)));
        directoryArrayList.add(new Directory(getString(R.string.agency_traffic), R.drawable.dr_traffic, getString(R.string.dir_link)));
        directoryArrayList.add(new Directory(getString(R.string.agency_vchicle_record), R.drawable.dr_vehicle, getString(R.string.dir_link)));
        directoryArrayList.add(new Directory(getString(R.string.agency_driving), R.drawable.dr_driving, getString(R.string.dir_link)));
        directoryArrayList.add(new Directory(getString(R.string.gtd_law), R.drawable.dr_traffic_law, APIs.PDF + "rethink.pdf"));
        directoryAdapter = new DirectoryAdapter(getActivity(), directoryArrayList);
        lvDirectory.setAdapter(directoryAdapter);
    }
}