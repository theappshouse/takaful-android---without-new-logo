package com.appshouse.takaful.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.AppController;
import com.appshouse.takaful.activities.MainActivity;
import com.appshouse.takaful.adapters.ClaimAdapter;
import com.appshouse.takaful.attributes.Claim;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 1/13/2016.
 */
public class ClaimListFragment extends Fragment {

    RecyclerView lvClaim;
    ArrayList<Claim> claimArrayList;
    RecyclerView.Adapter claimAdapter;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.list_claims, container, false);
        lvClaim = (RecyclerView) view.findViewById(R.id.lvClaims);
        lvClaim.setLayoutManager(new LinearLayoutManager(getActivity()));
        lvClaim.setHasFixedSize(true);

        getClaims();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((MainActivity) getActivity()).setAddClaimMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((MainActivity) getActivity()).setAddClaimMenu(false);
        AppController.getInstance().cancelPendingRequests(Tag_Request);
    }

    String Tag_Request = "getClaims";

    private void getClaims() {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetClaims
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                setClaims(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getClaims();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SharedPreferencesClass setting = new SharedPreferencesClass(getActivity());
                Log.i("userId", String.valueOf(setting.getUserId()));
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("cpr", setting.getCpr());
                params.put("deviceId", MyMethods.getDeviceId(getActivity()));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setClaims(JSONObject jsonObject) {
        try {
            Log.i("setClaims", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                claimArrayList = new ArrayList<>();
                JSONArray resultArray = jsonObject.getJSONArray("result");
                if (resultArray.length() > 0) {
                    for (int i = 0; i < resultArray.length(); i++) {
                        JSONObject claimObject = resultArray.getJSONObject(i);
                        String no = claimObject.getString("Claim No");
                        int status = claimObject.getInt("Status");
                        String reasonRejection = claimObject.getString("Rejection_Reason");

                        claimObject.remove("Claim No");
                        claimObject.remove("Status");
                        claimObject.remove("Rejection_Reason");
                        claimObject.remove("Nature of loss");

                        claimArrayList.add(new Claim(no, status, claimObject, reasonRejection));
                    }

                    claimAdapter = new ClaimAdapter(getActivity(), claimArrayList);
                    lvClaim.setAdapter(claimAdapter);
                } else {
                    view.findViewById(R.id.tvNoClaim).setVisibility(View.VISIBLE);
                }
            } else if (jsonObject.getInt("status") == 4) {
                MyMethods.showLoginPage(getActivity());
            } else {
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getClaims();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
